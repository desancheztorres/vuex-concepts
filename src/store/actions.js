import axios from 'axios'

export const fetchNumber = ({ commit }, { min, max }) => {
    axios.get(`http://localhost:8080/api/random?min=${min}&max=${max}`).then((response) => {
        commit('addRandomNumber', response.data.number)
    })
}